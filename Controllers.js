class Controllers extends Array {
    constructor() {
        super();
    }

    selectController(controllerName){
        this.forEach( (controller) => {
            controller.deselect()
        });
        let controller = this.controllerFromName(controllerName);
        controller.select();
        updateActiveController()
    }

    addControllerFromUi(){
        let controllerName = $("#controllerName").val();
        if(controllerName.length > 0 && typeof this.controllerFromName(controllerName) === "undefined"){
            let controller = new Controller();
            controller.name = controllerName;
            this.push(controller);
            updateActiveController();
            this.save()
        }
    }

    delController(controllerName){
        if(this.length > 1) {
            this.splice(this.indexOf(this.controllerFromName(controllerName)), 1);
            updateActiveController()
        }else{
            alert("There must be at least one Profile at any time.")
        }
    }

    controllerFromName(controllerName){
        return this.find( (controller) => {
            return controller.name === controllerName;
        });
    }

    get selected(){
        return this.find( (controller) => {
            return controller.selected === true;
        });
    }

    save(){
        save("controllers", controllers)
    }
}