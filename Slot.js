class Slot{
    id;
    status = "";
    description = "";
    percentdone = 0;
    parent;
    eta;
    device = "";
    reason ="";

    constructor(slotinfo, parent) {
        this.id = slotinfo.id;
        this.status = slotinfo.status;
        this.description = slotinfo.description;
        this.parent = parent
        this.device = this.description.split(":")[0]
    }

    update(slotinfo){
        this.status = slotinfo.status;
        this.reason = slotinfo.reason;
    }

    get info(){
        let state = "";
        let bg = "success"

        if(this.status === "PAUSED"){ bg= this.parent.connected ? "warning" : "secondary"; state += `<i class=\"fas fa-pause text-${bg}\" title=\"Paused. Not currently folding.\" ></i>`; }
        else if(this.status === "RUNNING"){ bg= this.parent.connected ? "success" : "secondary"; state += `<i class=\"fas fa-play text-${bg}\" title=\"Running\" ></i>`; }
        else if(this.status === "STOPPING"){ bg= this.parent.connected ? "warning" : "secondary"; state += `<i class=\"fas fa-hourglass-start text-${bg}\" title=\"Stopping. Will pause soon.\" ></i>`; }
        else if(this.status === "READY"){ bg= this.parent.connected ? "warning" : "secondary"; state += `<i class=\"fas fa-hourglass-start text-${bg}\" title=\"Ready. Will start soon.\" ></i>`; }
        else if(this.status === "FINISHING"){ bg= this.parent.connected ? "info" : "secondary"; state += `<i class=\"fas fa-step-forward text-${bg}\" title=\"Finishing. Will complete current Workunit, then pause.\" ></i>`; }

        state += ` <span title="${this.description}"> ${this.device.toUpperCase()}</span>`;

        state += "<div class=\"progress ml-3\" style=\"width: 50%; display: inline-flex; height: 1rem;\">\n" +
            `  <div class=\"progress-bar progress-bar-striped bg-${bg}\" role=\"progressbar\" style=\"width: ${(this.percentdone*100).toFixed(2)}%;\" aria-valuenow=\"${(this.percentdone*100).toFixed(2)}\" aria-valuemin=\"0\" aria-valuemax=\"100\">${(this.percentdone*100).toFixed(2)}%</div>\n` +
            "</div>"
        if(typeof this.eta !== "undefined") {
            let toArrival = this.eta.getTime() - (new Date).getTime()
            if (toArrival > 0) {
                state += ` ${Math.round(toArrival / 36000) / 100}h `
            }
        }

        return state
    }

    unitsFunc(unit){
        this.percentdone = Number(unit.percentdone.substring(0, unit.percentdone.length - 1)) / 100;
        this.eta = parseTime(unit.eta);
    }
}