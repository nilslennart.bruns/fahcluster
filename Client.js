class Client{
    id;
    parent;
    host = "";
    port = 0;
    name = "";
    passwd = "";
    user = "";
    socket = null;
    url;
    configured;
    authTries = 0;
    _authed = false;
    connected = false;
    lastHeartbeat = 0;
    slots = [];
    evaluators = {
        "configured": this.setConfigured.bind(this),
        "error":this.error.bind(this),
        "heartbeat": this.heartbeat.bind(this),
        "slots": this.slotsFunc.bind(this),
        "units": this.unitsFunc.bind(this),
    };

    toJSON(key){
        return {
            "host": this.host,
            "name": this.name,
            "port": this.port,
            "passwd": this.passwd,
        }
    }

    fromSave(entry, parent, id){
        this.id = id
        this.parent = parent;
        this.host = entry.host;
        this.port = entry.port;
        this.name = entry.name;
        this.passwd = entry.passwd;
    }

    async delete(){
        if(this.connected && this.authed){ this.send("exit"); }
        if(this.isConnAlive()) { this.socket.end(); }
        await this.parent.removeClient(this);
        delete this;
    }

    isConnAlive(){
        return this.socket !== null
    }

    fromArgs(name, host, port, passwd, parent, id){
        this.id = id;
        this.parent = parent;
        this.host = host;
        this.port = port;
        this.name = name;
        this.passwd = passwd;
    }

    auth(){
        return new Promise( (resolve, reject) => {
            if(typeof this.passwd === "string" && this.passwd.length > 0) {
                this.socket.on('data', (data) => {
                    if(data.toString().search("OK") >= 0 ){
                        this.passCorrect = true;
                        this.authed = true;
                        resolve()
                    }else{
                        let filter = /\nPyON (\d+) ([a-z0-9-_]+)\n([\w\d\s\n\[\{\"\'\]\}\s:,.%-]+?)\n---\n/g;
                        let matches = data.toString().matchAll(filter);
                        for (const match of matches) {
                            let [_, version, command, content] = match;
                            let value = eval(content.replace(/True/g, 'true').replace(/False/g, 'false').replace(/None/g, 'null').replace(/\'/g, "\""));
                            if(value === "Access denied, invalid password"){
                                this.passCorrect = false;
                                reject(null)
                            }
                        }
                    }
                })
                this.send("auth " + this.passwd);
                setTimeout(() => { reject(null) }, 4000)
            }else{
                this.authed = this.isAuthed();
                this.authed ? resolve() : reject(null)
            }
        })
    }

    connectProxy(){
        let msg = new Uint8Array([5, 1, 0,]);
        this.socket.write(msg)

        let ipv4 = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
        let type;
        let tuple;
        if (ipv4.test(this.host)) {
            type = 1;
            tuple = $.map(this.host.split("."), (entry) => {
                return Number(entry)
            })
        } else {
            type = 0;
            tuple = [0, 0, 0, 0]
        }

        msg = new Uint8Array([5, 1, 0,].concat([type]).concat(tuple).concat([141, 234,]));
        this.socket.write(msg);
    }

    async reConnect(){
        await this.disconnect();
        await this.connect();
        return
    }

    async connect(){
        if(this.connected){
            return
        }


        let postConnect = async function() {
            if(typeof this.parent.proxyHost == "string" && this.parent.proxyHost.length > 0){
                this.connectProxy()
            }

            this.connected = true;

            this.socket.on('end', function () {
                console.log('disconnected from server');
                this.socket = null;
                this.connected = false;
                this.authed = false;
                this.updateDisplay()
            }.bind(this));

            this.socket.on('error', function (err) {
                console.error({"client": this.name, "err": err});
                if (this.socket !== null) {
                    this.socket.end();
                    this.socket = null;
                    this.authed = false;
                }
                this.connected = false;
                this.updateDisplay()
            }.bind(this))

            await this.auth();
            this.socket.on('data', function (data) {
                let filter = /\nPyON (\d+) ([a-z0-9-_]+)\n([\w\d\s\n\[\{\"\'\]\}\s:,.%-]+?)\n---\n/g;
                let matches = data.toString().matchAll(filter);
                for (const match of matches) {
                    let [_, version, command, content] = match;
                    let value = eval(content.replace(/True/g, 'true').replace(/False/g, 'false').replace(/None/g, 'null').replace(/\'/g, "\""));
                    this.evalMessage(command, value)
                }
            }.bind(this));
        }.bind(this);

        if(typeof this.parent.proxyHost == "string" && this.parent.proxyHost.length > 0) {
            this.socket = socket.connect({host: this.parent.proxyHost, port: this.parent.proxyPort}, postConnect)
        }else{
            this.socket = socket.connect({host: this.host, port: this.port}, postConnect)
        }


    }

    async isAuthed(){
        if(!this.connected){ return false }
        if(typeof this.socket._events.data !== "undefined"){ let oldOnData = this.socket._events.data[0];}
        this.send("add 1 1");
        let authPromise = new Promise((resolve, reject) => {
            let failTimeout = setTimeout(() => { resolve(false);}, 1000)
            this.socket.on('data', (data) => {
                if(data.toString().trim() === "2")
                    clearTimeout(failTimeout)
                resolve(true);
            });
        });
        await authPromise;
        if(typeof oldOnData !== "undefined"){ this.socket.on('data', oldOnData);}
        return true;
    }

    get fullHost(){
        return this.host + ":" + this.port;
    }

    get displayName(){
        let displayName = this.name;
        if(this.connected) {
            if (!this.authed) {
                displayName = "<i class=\"fas fa-key text-danger\" title=\"Could not Authenticate. Password may be wrong.\"></i> " + displayName
            }
            if (!this.getConfigured() && this.authed) {
                displayName = "<i class=\"fas fa-user text-danger\" title=\"Client is Anonymous\"></i> " + displayName
            }
        }else{
            displayName = `<i class="text-dark" title="Not connected."><i class="fas fa-unlink"></i> ${displayName} </i>`
        }
        return displayName
    }

    get authed(){ return this._authed}

    set authed( value ){
        if(!this._authed && this.authed !== value){
            this.initUpdates()
        }
        this._authed = value
    }

    get info(){
        let info = "";
        this.slots.forEach( (slot) => {
            info += slot.info + "<br>";
        })
        return info
    }

    async initUpdates(){
        this.send("updates clear");
        this.send("updates add 0 5 $heartbeat");
        //send("updates add 1 1 $(options <a list of option names> *)");
        this.send("updates add 2 4 $queue-info");
        this.send("updates add 3 1 $slot-info");
        this.send("heartbeat");
        this.send("queue-info");
        this.send("slot-info");
    }

    async setConfigured(value){
        this.configured = value;
    }

    async getConfigured(){
        if(typeof this.configured !== "boolean"){
            this.send("configured");
            return false;
        }else{
            return this.configured
        }
    }

    async updateDisplay(){
        //this.parent.clientTable.bootstrapTable("updateCellByUniqueId", {id: this.id, field:"info", value: this.info})
        this.parent.clientTable.DataTable().row(`${this.id}`).invalidate()
    }


    /* All functions in reaction to receiving data */

    async evalMessage(command, value){
        if(!(command in this.evaluators)){ console.error(`Unknown Response! ${command} : ${value} `); return }
        this.evaluators[command](value);
    }

    async error(value){
        console.error({clientname: this.name, error:value })
    }

    async slotsFunc(value){
        value.forEach( (entry) => {
            let slot = this.slots.find(function (slot) {
                return slot.id === entry.id
            }.bind(entry))
            if(typeof slot === "undefined"){
                slot = new Slot(entry, this)
                this.slots.push(slot)
                this.updateDisplay()
            }else{
                slot.update(entry)
            }
        })
    }

    async unitsFunc(value){
        value.forEach( (entry) => {
            let slot = this.slots.find(function (slot) {
                return slot.id === entry.slot
            }.bind(entry))
            if(typeof slot !== "undefined"){
                slot.unitsFunc(entry)
            }
        })
    }

    async heartbeat(){
        this.lastHeartbeat = new Date()
    }


    /* All commands to the remote clients */

    async shutdown(){
        this.send("shutdown")
    }

    async send(message){
        if(this.isConnAlive()) {
            this.socket.write(`${message}\n`)
        }
    }

    async setOption(cParam, value){
        this.send(`option ${cParam} ${value}`)
    }

    async pause(action, device){
        if(typeof device === "undefined") {
            if (action === true) {
                this.send("pause")
            } else {
                this.send("unpause")
            }
            this.send("slot-info")
        }else{
            this.slots.forEach((slot) => {
                if(slot.device === device){
                    if (action === true) {
                        this.send(`pause ${slot.id}`)
                    }else{
                        this.send(`unpause ${slot.id}`)
                    }
                }
            })
        }

    }

    async finish(device){
        if(typeof device === "undefined") {
            this.send("finish");
        }else{
            this.slots.forEach((slot) => {
                if(slot.device === device){
                    this.send(`finish ${slot.id}`)
                }
            })
        }
        this.send("slot-info")
    }

    async disconnect(){
        this.send("exit");
        if(this.socket !== null && typeof this.socket !== "undefined" ) await this.socket.end(); this.socket = null;
        this.connected = false;
        this.authed = false;
        return true
    }

    /* ---------------------------------------- */
}