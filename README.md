# Folding@Home Cluster
Similar to [Folding@Home](https://foldingathome.org/) Controller but geared more to managing hundreds of clients at once.
This is a **third-party** application uses the [FAHClient API](https://github.com/FoldingAtHome/fah-control/wiki/3rd-party-FAHClient-API)

# Features
* Import & Export clients as csv to add hundreds of clients easily
* Run, Pause, Configure all clients simultaneously
* Charts for client stats
* Multiple profiles with different proxies
* Socks5-proxy support

## Relase
* Linux [dist/FAHCluster-linux-x64_1.0.zip](https://gitlab.com/nilslennart.bruns/fahcluster/-/raw/master/dist/FAHCluster-linux-x64_1.0.zip?inline=false)
* Windws [dist/FAHCluster-win32-x64_1.0.zip](https://gitlab.com/nilslennart.bruns/fahcluster/-/raw/master/dist/FAHCluster-win32-x64_1.0.zip?inline=false)
* MAC dist/FAHCluster-darwin-x64_1.0.zip (Never tested)

## Beta 
The Application is still in Beta. Many features are still missing.
The maximum amount of clients tested is 350. (clients were emulated)

## Warning!
THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.