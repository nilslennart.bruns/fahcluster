class Controller{
    clientTable;
    clients = [];
    selected = false;
    name = "";
    proxyHost = "";
    proxyPort = "";
    updateLoopRunning = false;

    constructor(save){
        if(typeof save === "object") {
            this.name = save.name;
            this.selected = save.selected;
            this.proxyHost = save.proxyHost;
            this.proxyPort = save.proxyPort;
            this.loadClients(save.clients);
        }
    }

    async select(){

        this.selected = true;
        controllers.save()
        this.initClientTable();
        this.initCharts();
        this.reConnect();
        this.reloadClientTable();
        this.updateLoop()
        this.updateLoopRunning = true;
    }

    async deselect(){
        this.selected = false
        this.updateLoopRunning = false
        try{ this.connectionChart.destroy() } catch (e) {};
        this.disconnect()
    }

    toJSON(key){
        return {
            "clients": this.clients,
            "name": this.name,
            "selected": this.selected,
            "proxyHost": this.proxyHost,
            "proxyPort": this.proxyPort,
        }
    }

    initClientTable(){
        this.clientTable = $('#clientTable');
        this.clientTable.DataTable({
            "bDestroy": true,
            data: this.clients,
            rowId: 'id',
            columnDefs: [
                {
                    targets:   0,
                    orderable: false,
                    className: 'select-checkbox',
                    data: null,
                    //visible: false,
                    defaultContent: '',
                },{
                    targets: 2,
                    className: 'cell-border'
                }
            ],
            select: {
                style:    'os',
                selector: 'td:first-child'
            },
            columns:[
                { data: "" },
                { data: "displayName"},
                { data: "fullHost"},
                { data: "info"}

            ],
        });

        this.clientTable.DataTable().on('select deselect',
            function () {
                $("#remove, #shutdown").prop('disabled', !this.selectedClients.length)
                if(this.selectedClients.length){
                    $("#crow").removeClass("d-none");
                    this.reloadConfig();
                }else{
                    $("#crow").addClass("d-none");
                }
            }.bind(this));

        $("#remove").click(function () {
            this.selectedClients.forEach( (client) => {
                client.delete()
            });
            if(this.selectedClients.length){
                $("#crow").removeClass("d-none");
                this.reloadConfig();
            }else{
                $("#crow").addClass("d-none");
            }
        }.bind(this));

        $("#shutdown").click(function () {
            this.selectedClients.forEach( (client) => {
                client.shutdown()
            });
        }.bind(this));

        $("#connect").click(function () {
            this.selectedClientsOrAll.forEach( (client) => {
                client.connect()
            })
        }.bind(this));
        $("#disconnect").click(function () {
            this.selectedClientsOrAll.forEach( (client) => {
                client.disconnect()
            })
        }.bind(this));

        $("#pause").click(function () {
            this.selectedClientsOrAll.forEach( (client) => {
                client.pause(true)
            })
        }.bind(this));
        $("#pauseCPU").click(function () {
            this.selectedClientsOrAll.forEach( (client) => {
                client.pause(true, "cpu")
            })
        }.bind(this));
        $("#pauseGPU").click(function () {
            this.selectedClientsOrAll.forEach( (client) => {
                client.pause(true, "gpu")
            })
        }.bind(this));

        $("#unpause").click(function () {
            this.selectedClientsOrAll.forEach( (client) => {
                client.pause(false)
            })
        }.bind(this));
        $("#unpauseCPU").click(function () {
            this.selectedClientsOrAll.forEach( (client) => {
                client.pause(false, "cpu")
            })
        }.bind(this));
        $("#unpauseGPU").click(function () {
            this.selectedClientsOrAll.forEach( (client) => {
                client.pause(false, "gpu")
            })
        }.bind(this));

        $("#finish").click(function () {
            this.selectedClientsOrAll.forEach( (client) => {
                client.finish()
            })
        }.bind(this));
        $("#finishCPU").click(function () {
            this.selectedClientsOrAll.forEach( (client) => {
                client.finish("cpu")
            })
        }.bind(this));
        $("#finishGPU").click(function () {
            this.selectedClientsOrAll.forEach( (client) => {
                client.finish("gpu")
            })
        }.bind(this));
    }

    initCharts(){
        let ctx = document.getElementById('connectionChart').getContext('2d');
        this.connectionChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Not Connected', 'Not Authenticated', 'Running', 'Finishing', 'Paused', 'Stopping', 'Ready'],
                datasets: [{
                    data: [1, 1, 1, 1, 1, 1, 1],
                    backgroundColor: [
                        'rgba(173, 181, 189, 0.2)',
                        'rgba(231, 76, 60, 0.2)',
                        'rgba(0, 188, 140, 0.2)',
                        'rgba(52, 152, 219, 0.2)',
                        'rgba(243, 156, 18, 0.2)',
                        'rgba(243, 156, 18, 0.2)',
                        'rgba(243, 156, 18, 0.2)',
                    ],
                    borderColor: [
                        'rgba(173, 181, 189, 1)',
                        'rgba(231, 76, 60, 1)',
                        'rgba(0, 188, 140, 1)',
                        'rgba(52, 152, 219, 1)',
                        'rgba(243, 156, 18, 1)',
                        'rgba(243, 156, 18, 1)',
                        'rgba(243, 156, 18, 1)',
                    ],
                    borderWidth: 1,
                }],
            },
        });
    }

    get selectedClients(){
        return this.clientTable.DataTable().rows( { selected: true } ).data().toArray()
        // this.clientTable.bootstrapTable('getSelections')
    }

    get selectedClientsOrAll(){
        let selected = this.selectedClients;
        if(selected.length === 0){
            return this.clients
        }
        return selected
    }

    stateFooter(){
        let states = {
            total: 0,
            connected: 0,
            authed: 0,
            configured: 0
        };
        this.clients.forEach( (client) => {
            states.total += 1;
            states.connected += client.connected;
            states.authed += client.authed;
            states.configured += client.configured;
        })
        let footer = `Total: ${states.total}<br>`;
        footer += `Connected: ${states.connected}<br>`;
        footer += `Not connected: ${states.total - states.connected}<br>`;

        if(states.authed < states.total){
            footer += `Not connected: ${states.total - states.authed}<br>`;
        }

        if(states.configured < states.total){
            footer += `Not configured: ${states.total - states.configured}<br>`;
        }
        return footer
    }

    reloadConfig(){

    }

    async updateLoop(){
        this.upadteCharts()
        for (let client of this.clients){
            await client.updateDisplay()
        }
        if(this.updateLoopRunning) setTimeout(this.updateLoop.bind(this), 1000)
        this.clientTable.DataTable().columns.adjust()
    }

    async upadteCharts(){
        this.connectionChart.data.datasets[0].data = [
            this.clients.filter(client => !client.connected).length,
            this.clients.filter(client => client.connected && !client.authed).length,
            this.slots.filter(slot => slot.status === "RUNNING" && slot.parent.connected && slot.parent.authed ).length,
            this.slots.filter(slot => slot.status === "FINISHING" && slot.parent.connected && slot.parent.authed).length,
            this.slots.filter(slot => slot.status === "PAUSED" && slot.parent.connected && slot.parent.authed).length,
            this.slots.filter(slot => slot.status === "STOPPING" && slot.parent.connected && slot.parent.authed).length,
            this.slots.filter(slot => slot.status === "READY" && slot.parent.connected && slot.parent.authed).length
        ];
        this.connectionChart.update()
    }

    get slots(){
        return this.clients.reduce((current, client) => current.concat(client.slots), [])
    }

    async loadClients(saveClients) {
        saveClients.forEach((entry) => {
            let client = new Client();
            client.fromSave(entry, this, this.newClientId());
            this.clients.push(client);
        });
    }

    async reConnect(){
        this.clients.forEach(function(client){
            client.connect();
        })
    }

    async disconnect(){
        this.clients.forEach(function(client){
            client.disconnect();
        })
    }

    reloadClientTable(){
        this.clientTable.DataTable().clear()
        this.clientTable.DataTable().rows.add(this.clients);
        this.clientTable.DataTable().draw()
        //this.clientTable.bootstrapTable('load', this.clients)
    }

    async addClient(client){
        this.clients.push(client);
        this.reloadClientTable();
        controllers.save()
    }

    async removeClient(client){
        this.clients.splice(this.clients.indexOf(client), 1);
        this.reloadClientTable();
        save('controllers', controllers);
    }

    newClientId(){
        if(this.clients.length > 0){
            return this.clients.slice(-1)[0].id + 1
        }else{
            return 0
        }
    }
}